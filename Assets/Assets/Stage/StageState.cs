using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newStageState", menuName = "StageState")]
public class StageState : ScriptableObject
{

    private bool _completed = false;
    [SerializeField] private List<Ballon> _ballons = new List<Ballon>();
    private int _numberOfBallonsLeft;
    private int _stageNumber;

    public bool Completed { get => _completed; set => _completed = value; }
    public List<Ballon> Ballons { get => _ballons; set => _ballons = value; }
    public int NumberOfBallonsLeft { get => _numberOfBallonsLeft; set => _numberOfBallonsLeft = value; }
    public int StageNumber { get => _stageNumber; set => _stageNumber = value; }

    public void Init(int StageNumber)
    {
        this.StageNumber = StageNumber;

        _ballons = GenerateBallons(this.StageNumber);
    }

    public List<Ballon> GenerateBallons(int _stageNumber)
    {
        List<Ballon> res = new List<Ballon>();

        for (int i = 0; i < Mathf.Pow(_stageNumber, 1.1f); i++)
        {
            res.Add(CreateNewBallon(PopCondition.FREE));

            _numberOfBallonsLeft++;
        }

        return res;

    }

    private Ballon CreateNewBallon(PopCondition popCondition)
    {
        Ballon res = new Ballon();

        res.data.Color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        res.data.PopCondition = popCondition;

        return res;
    }

    public void CheckForCompletion()
    {
        if (_ballons.Count <= 0)
        {
            _completed = true;
        }
    }

}

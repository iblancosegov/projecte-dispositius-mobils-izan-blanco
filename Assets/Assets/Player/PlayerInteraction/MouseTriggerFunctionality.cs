using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseTriggerFunctionality : MonoBehaviour
{
    public event Action<Vector2> OnMouseTriggerEnter;
    public event Action<Vector2> OnMouseTriggerExit;
    public event Action<Vector2> OnMouseTriggerStay;
    // Start is called before the first frame update

    private void OnTriggerEnter2D(Collider2D collision)
    {
        OnMouseTriggerEnter?.Invoke(transform.position);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        OnMouseTriggerExit?.Invoke(transform.position);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        OnMouseTriggerStay?.Invoke(transform.position);
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.U2D;

public class PlayerInteraction : MonoBehaviour
{

    public event Action onSuccesfulCut;

    private TrailRenderer trailRenderer;
    private PlayerStatus playerStatus;
    private MouseTriggerFunctionality mtf;
    private Transform ballonReference;



    private List<Vector2> cutList;
    private Vector2 cutStartingPoint;
    private Vector2 cutEndingPoint;

    private float cutTolerance;
    // Start is called before the first frame update
    void Start()
    {
        trailRenderer = GetComponentInChildren<TrailRenderer>();
        playerStatus = GetComponent<PlayerStatus>();
        mtf = GetComponentInChildren<MouseTriggerFunctionality>();
        ballonReference = transform.GetChild(1); //Get the reference ballon's transform.

        cutList = new List<Vector2>();

        mtf.OnMouseTriggerEnter += OnMouseTriggerEnter;
        mtf.OnMouseTriggerExit += OnMouseTriggerExit;
        mtf.OnMouseTriggerStay += OnMouseTriggerStay;

        cutTolerance = 0.95f;

        DontDestroyOnLoad(this.gameObject);
    }

    public IEnumerator OnMouseHeldCoroutine()
    {
        while (true)
        {
            Vector3 mousePos = Mouse.current.position.ReadValue();
            trailRenderer.transform.position = GameManager.Instance.Camera.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 1.0f));

            yield return null;
        }

    }



    public void StartTrailRendering()
    {
        if(!GameManager.Instance.SceneManipulator.IsInShop())
        trailRenderer.emitting = true;
    }
    public void StopTrailRendering()
    {
        if(!GameManager.Instance.SceneManipulator.IsInShop())
        trailRenderer.emitting = false;
    }

    private void OnMouseTriggerEnter(Vector2 collisionPoint)
    {
        if (playerStatus.IsHolding)
        {
            cutStartingPoint = trailRenderer.transform.position;
        }

    }

    private void OnMouseTriggerStay(Vector2 collisionPoint)
    {
        if (playerStatus.IsHolding)
        {
            cutList.Add(trailRenderer.transform.position);
        }
    }

    private void OnMouseTriggerExit(Vector2 collisionPoint)
    {
        if (playerStatus.IsHolding)
        {
            cutEndingPoint = trailRenderer.transform.position;

            CheckForSuccesfulCutV2();

            cutList.Clear();
        }
    }

    private void CheckForSuccesfulCutV2()
    {

        Vector2 middlePoint = cutList[cutList.Count / 2];

        Vector2 direction = (middlePoint - cutStartingPoint).normalized;

        Vector2 direction2 = (cutEndingPoint - middlePoint).normalized;



        float dot = Vector2.Dot(direction2, direction);

        print(dot);


        if (dot >= cutTolerance)
        {
            print("Yes");
            onSuccesfulCut?.Invoke();
        }

        else
        {
            print("No");
        }

    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerStatus : MonoBehaviour
{
    private bool _isHolding;

    public bool IsHolding { get => _isHolding; }
    public PlayerStats Stats { get => _stats; set => _stats = value; }

    private PlayerStats _stats;

    private PlayerInteraction _playerInteraction;


    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.PlayerInputManager.inputActions.Player.Hold.performed += playerMousePressed;

        GameManager.Instance.PlayerInputManager.inputActions.Player.Hold.canceled += playerMouseReleased;

        GameManager.Instance.StageManager.onBallonPopped += ObtainGold;

        _stats = new PlayerStats();

        _playerInteraction = GetComponent<PlayerInteraction>();

        StartCoroutine(_playerInteraction.OnMouseHeldCoroutine());
    }

    private void playerMousePressed(InputAction.CallbackContext ctx)
    {

        _isHolding = true;
        _playerInteraction.StartTrailRendering();


        Collider2D[] collisions = Physics2D.OverlapCircleAll(Mouse.current.position.ReadValue(), 1.5f);


        print(Mouse.current.position.ReadValue());

        foreach (Collider2D collision in collisions)
        {
            print(collision);


            if(collision.name == "Ballon")
            {
                _isHolding = false;
                _playerInteraction.StopTrailRendering();
            }
        }

    }

    private void playerMouseReleased(InputAction.CallbackContext ctx)
    {
        _isHolding = false;

        _playerInteraction.StopTrailRendering();
    }

    private void ObtainGold(int gold)
    {
        _stats.Currency += gold;
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats
{
    private int _slicePower;

    private int _currency;

    private int _goldMultiplier;
    public int SlicePower { get => _slicePower; set => _slicePower = value; }
    public int Currency { get => _currency; set => _currency = value; }

    public int GoldMultiplier { get => _goldMultiplier; set => _goldMultiplier = value; }

    public PlayerStats()
    {
        _slicePower = 1;
        _currency = 0;
        _goldMultiplier = 1;
    }
}

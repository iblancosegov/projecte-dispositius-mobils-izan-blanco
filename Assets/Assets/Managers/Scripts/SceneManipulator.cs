using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManipulator : MonoBehaviour
{

    private bool _inShop;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public bool IsInShop()
    {
        return _inShop;
    }

    public void SetIsInShop(bool isInShop)
    {
        _inShop = isInShop;
    }

    public void GoToShop()
    {
        SceneManager.LoadScene("ShopScene");
    }

    public void GoToMain()
    {
        SceneManager.LoadScene("MainScene");
    }
}

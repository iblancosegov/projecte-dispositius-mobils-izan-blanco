using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManagerWrapper : MonoBehaviour
{
    SceneManipulator m_Manipulator;

    [SerializeField] private GameObject _goldDisplay;

    // Start is called before the first frame update
    void Start()
    {
        Init();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Init()
    {
        m_Manipulator = GameManager.Instance.SceneManipulator;
        GameManager.Instance.ShopManager._playerCurrencyDisplayObject = _goldDisplay;
        GameManager.Instance.ShopManager.RefreshUserGold();
    }

    public void GoToShop()
    {
        m_Manipulator.GoToShop();
    }

    public void GoToMain()
    {
        m_Manipulator.GoToMain();
    }
}

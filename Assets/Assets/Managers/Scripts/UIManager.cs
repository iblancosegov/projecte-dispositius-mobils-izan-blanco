using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{

    [SerializeField] private GameObject _ballonCount;

    [SerializeField] private GameObject _stageNumber;

    public GameObject BallonCount { get => _ballonCount; set => _ballonCount = value; }
    public GameObject StageNumber { get => _stageNumber; set => _stageNumber = value; }

    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.StageManager.onStageStarted += RefreshStageNumber;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void RefreshStageNumber(int _stageNumber)
    {
        StageNumber.GetComponent<TextMeshProUGUI>().text = string.Format("Stage {0}", _stageNumber.ToString());
    }

    public void RefreshBallonCount(int gold)
    {
        BallonCount.GetComponent<TextMeshProUGUI>().text = GameManager.Instance.StageManager.CurrentState.NumberOfBallonsLeft.ToString();
    }
}

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ShopManager : MonoBehaviour
{
    public GameObject _playerCurrencyDisplayObject;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RefreshUserGold()
    {
        _playerCurrencyDisplayObject.GetComponent<TextMeshProUGUI>().text = GameManager.Instance.PlayerStatus.Stats.Currency.ToString();
    }

    
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputManager : MonoBehaviour
{
    public PlayerInteractionControls inputActions;

    private void OnEnable()
    {
        inputActions = new PlayerInteractionControls();
        inputActions.Enable();
    }

    private void OnDisable()
    {
        inputActions = null;
    }
}

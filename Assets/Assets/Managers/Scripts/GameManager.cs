using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public Vector2 ScreenCenter { get; private set; }

    [field: SerializeField] public PlayerInputManager PlayerInputManager { get; private set; }

    [field: SerializeField] public PlayerInteraction PlayerInteraction { get; private set; }

    [field: SerializeField] public PlayerStatus PlayerStatus { get; private set; }

    [field: SerializeField] public StageManager StageManager { get; private set; }

    [field: SerializeField] public UIManager UIManager { get; private set; }

    [field: SerializeField] public ShopManager ShopManager { get; private set; }

    [field: SerializeField] public SceneManipulator SceneManipulator { get; private set; }

    [field: SerializeField] public Camera Camera { get; set; }


    private void Awake()
    {

        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(this.transform.parent.gameObject);
        }
    }

    private void Start()
    {
        PlayerInteraction.onSuccesfulCut += StageManager.PopBallon;

        ScreenCenter = Camera.ViewportToWorldPoint(new Vector2(0.5f, 0.5f));

        print(ScreenCenter);

        StageManager.onBallonPopped += UIManager.RefreshBallonCount;

        StageManager.onStageStarted += UIManager.RefreshBallonCount;

    }
}

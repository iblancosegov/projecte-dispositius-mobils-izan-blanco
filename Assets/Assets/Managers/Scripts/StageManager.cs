using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageManager : MonoBehaviour
{

    public event Action<int> onBallonPopped;

    public event Action onStageCompleted;

    public event Action<int> onStageStarted;

    private StageState _currentState = null;
    private GameObject _physicalBallonList;
    [SerializeField] private GameObject _ballonPrefab;
    private int _stageNumber = 1;

    public StageState CurrentState { get => _currentState; set => _currentState = value; }
    public int StageNumber { get => _stageNumber; set => _stageNumber = value; }




    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Setup()
    {
        _physicalBallonList = new GameObject();

        _physicalBallonList.name = "BallonList";

        _physicalBallonList.transform.position = GameManager.Instance.ScreenCenter;

        StartCoroutine(SetupNextStageCoRoutine(StageNumber));
    }

    

    private bool IsCurrentStageCompleted() 
    {
        return CurrentState.Completed;
    }

    /*private void SetupNextStage(int StageNumber)
    {
        StageState res = new StageState(StageNumber);

        _currentState = res;

        ShowBallon();

        onStageStarted?.Invoke();
    }*/

    IEnumerator SetupNextStageCoRoutine(int StageNumber)
    {

        yield return new WaitForSeconds(.5f);

        if(GameManager.Instance.SceneManipulator.IsInShop())
        {
            yield break;
        }

        StageState res = ScriptableObject.CreateInstance<StageState>();

        res.Init(StageNumber);

        _currentState = res;

        ShowBallon();

        onStageStarted?.Invoke(StageNumber);
    }

    IEnumerator SetupNextStageCoRoutine(StageState stageState)
    {
        StageState res = stageState;

        onStageStarted?.Invoke(stageState.StageNumber);

        yield return null;
    }




    private void ShowBallon()
    {
        Ballon ballon = new Ballon(_currentState.Ballons[0].data);
        GameObject newBallonObject = Instantiate(_ballonPrefab);

        //Returns the color blue;

        newBallonObject.GetComponent<SpriteRenderer>().color = ballon.data.Color;

        newBallonObject.transform.parent = _physicalBallonList.transform;
    }

    public void PopBallon()
    {

        int _slicePower = GameManager.Instance.PlayerStatus.Stats.SlicePower;

        int goldGained = 0;

        int goldMult = GameManager.Instance.PlayerStatus.Stats.GoldMultiplier;

        Destroy(_physicalBallonList.transform.GetChild(0).gameObject);
        

        if (_currentState.Ballons.Count < _slicePower)
        {
            _currentState.Ballons.Clear();
            _currentState.NumberOfBallonsLeft = 0;
            goldGained = _currentState.Ballons.Count * goldMult;
        }
        else 
        {
            _currentState.Ballons.RemoveRange(0, _slicePower);
            _currentState.NumberOfBallonsLeft -= _slicePower;
            goldGained = _slicePower * goldMult;
        }

        
        onBallonPopped?.Invoke(goldGained);

        GameManager.Instance.ShopManager.RefreshUserGold();


        _currentState.CheckForCompletion();

        if (_currentState.Completed)
        {
            onStageCompleted?.Invoke();

            print(string.Format("Stage {0} done.", StageNumber));
            StageNumber++;
            StartCoroutine(SetupNextStageCoRoutine(StageNumber));
        }
        else {
            ShowBallon();
        }
    }
}

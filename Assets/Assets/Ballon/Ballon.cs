using System.Collections;
using System.Collections.Generic;
using UnityEditor.ShaderGraph;
using UnityEngine;

public class Ballon
{
    public BallonData data;

    public Ballon()
    {
        data = ScriptableObject.CreateInstance<BallonData>();

    }

    public Ballon(BallonData data)
    {
        this.data = data;

    }

    public Ballon(Color color, PopCondition popCondition)
    {

        data = new BallonData();

        data.Color = color;
        data.PopCondition = popCondition;
    }

    public void Initialize(BallonData ballonData)
    {
        data = ballonData;
    }

    public void Apply(GameObject gameObject)
    {
        gameObject.GetComponent<SpriteRenderer>().color = data.Color;
    }
}

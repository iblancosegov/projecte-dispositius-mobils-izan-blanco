using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
[CreateAssetMenu(fileName = "BallonData")]
public class BallonData : ScriptableObject
{
    public Color Color;
    public PopCondition PopCondition;

    private void OnEnable()
    {
        Color = Color.red;
        PopCondition = PopCondition.FREE;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum PopCondition
{
    FREE, DIRECTIONAL, PRECISE_DIRECTIONAL, COMPLEX_PATTERN
}

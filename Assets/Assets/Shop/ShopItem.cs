using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

abstract public class ShopItem : MonoBehaviour
{

    [SerializeField] public ShopItemData _shopItemData;

    [SerializeField] private GameObject _nameObject;

    [SerializeField] private GameObject _descriptionObject;

    [SerializeField] private GameObject _priceObject;

    [SerializeField] private GameObject _buttonObject;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void RefreshInformation()
    {
        _nameObject.GetComponent<TextMeshProUGUI>().text = _shopItemData.Name;
        _descriptionObject.GetComponent<TextMeshProUGUI>().text= _shopItemData.Description;
        _priceObject.GetComponent<TextMeshProUGUI>().text = _shopItemData.Price.ToString();
    }

    abstract public void GenerateDescription();

    abstract public void GeneratePrice();

    abstract public void OnBuy();

    public void RefreshShopGoldDisplay()
    {
        GameObject.FindGameObjectWithTag("PlayerCurrency").GetComponent<TextMeshProUGUI>().text = GameManager.Instance.PlayerStatus.Stats.Currency.ToString();
    }

    public void OnTryBuy()
    {

        print(GameManager.Instance.PlayerStatus.Stats.Currency);
        print(_shopItemData.Price);
        if (GameManager.Instance.PlayerStatus.Stats.Currency >= _shopItemData.Price)
        {
            print("COMPRABLE");   
            OnBuy();
            RefreshInformation();
        }
    }


}

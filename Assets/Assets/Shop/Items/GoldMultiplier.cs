using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldMultiplier : ShopItem
{
    public override void GenerateDescription()
    {
        _shopItemData.Description = string.Format("For every ballon you pop gain {0} (+ 1) gold.", GameManager.Instance.PlayerStatus.Stats.GoldMultiplier);
    }

    public override void GeneratePrice()
    {
        _shopItemData.Price = ((int)Mathf.Pow(GameManager.Instance.PlayerStatus.Stats.GoldMultiplier, 1.25f) * 15);
    }



    public override void OnBuy()
    {
        GameManager.Instance.PlayerStatus.Stats.GoldMultiplier++;
        GameManager.Instance.PlayerStatus.Stats.Currency -= _shopItemData.Price;

        GeneratePrice();
        GenerateDescription();

        GameManager.Instance.ShopManager.RefreshUserGold();
    }

    // Start is called before the first frame update
    void Start()
    {
        GeneratePrice();
        GenerateDescription();
        RefreshInformation();
    }

    // Update is called once per frame
    void Update()
    {

    }
}

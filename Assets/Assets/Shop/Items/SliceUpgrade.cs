using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliceUpgrade : ShopItem
{
    public override void GenerateDescription()
    {
        _shopItemData.Description = string.Format("Upgrades your slice power to cut trough {0} (+ 1) ballons at once.", GameManager.Instance.PlayerStatus.Stats.SlicePower);
    }

    public override void GeneratePrice()
    {
        _shopItemData.Price = ((int)Mathf.Pow(GameManager.Instance.PlayerStatus.Stats.SlicePower, 1.05f) * 10);
    }



    public override void OnBuy()
    {
        GameManager.Instance.PlayerStatus.Stats.SlicePower++;
        GameManager.Instance.PlayerStatus.Stats.Currency -= _shopItemData.Price;

        GeneratePrice();
        GenerateDescription();

        GameManager.Instance.ShopManager.RefreshUserGold();
    }

    // Start is called before the first frame update
    void Start()
    {
        GeneratePrice();
        GenerateDescription();
        RefreshInformation();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

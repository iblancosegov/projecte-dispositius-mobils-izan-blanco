using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopSetup : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    [SerializeField] private GameObject _userGoldDisplay;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.Camera = _camera;
        GameManager.Instance.ShopManager._playerCurrencyDisplayObject = _userGoldDisplay;
        GameManager.Instance.ShopManager.RefreshUserGold();
        GameManager.Instance.SceneManipulator.SetIsInShop(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainSetupScript : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    [SerializeField] private GameObject _StageNumber;
    [SerializeField] private GameObject _BallonsLeft;
    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.Camera = _camera;
        GameManager.Instance.StageManager.Setup();

        GameManager.Instance.UIManager.StageNumber = _StageNumber;
        GameManager.Instance.UIManager.BallonCount = _BallonsLeft;

        GameManager.Instance.SceneManipulator.SetIsInShop(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
